//Load required packages
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var User = new mongoose.Schema({
	email: { type: String, unique: true, required:true},
	password: {type: String, required: true}
});

User.pre('save', function(callback){
	var user = this;

	if(!user.isModified('password')){
		return callback();
	}

	bcrypt.genSalt(5, function(err, salt){
		if(err){
			return callback(err);
		}

		bcrypt.hash(user.password, salt, null, function(err, hash){
			if(err){
				return callback(err);
			}

			user.password = hash;			
		});
	});
});

User.methods.verifyPassword = function(password, callback){
	bcrypt.compare(password, this.password, function(err, isMatch){
		if(err){
			return callback(err);
		}

		callback(null, isMatch);
	});
}

//Export the mongoose model
module.exports = mongoose.model('User', User);