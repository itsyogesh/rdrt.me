var mongoose = require('mongoose');

var Url = new mongoose.Schema({
	user_id: {type:String, required: true },
	app_id: {type: String, required: true},
	base: {type: String, required: true},
	web_url: {type: String},
	android_url: {type: String},
	ios_url: {type: String},
	windows_url: {type: String},
	preferences: {}
});

module.exports = mongoose.model('Url', Url);