
//Load modules
var rdrtApp = angular.module('rdrtApp', ['ui.router', 'ngResource', 'angular-loading-bar', "slugifier"]);

//Routes
rdrtApp.config(function ($stateProvider) {
	
	$stateProvider
		.state('home', {
			url: "/",
			templateUrl: "templates/overview.html"
		})
		.state('newApp', {
			url: "/apps/new",
			templateUrl: "templates/create-app.html",
			controller: "AppController"
		});
});

//Controllers
rdrtApp.controller('AppController', ['$scope', '$location','Slug', function($scope, $location, Slug){
	$scope.appName = "";
	$scope.baseUrl = Slug.slugify($scope.appName) || "";
	$scope.androidAppUrl = "";
	$scope.iosAppUrl = "";
	$scope.windowsAppUrl = "";
}]);