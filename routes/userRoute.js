var User = require('../models/user');

exports.addUser = function(req, res){
	var user = new User();
	user.email = req.body.email;
	user.password = req.body.password;

	user.save(function(err){
		if(err){
			res.send(err);
		}

		res.json({
			message: 'User added successfully'
		});
	});
};

exports.updateUser = function(req, res){
	User.findById(req.body_id, function(err, user){
		user.email = (req.body.email) ? req.body.email : user.email;
		if(req.body.password){
			user.password = req.body.password;
		}

		user.save(function(err){
			if(err){
				res.send(err);
			}

			res.json({
				message: "user updated successfully"
			});
		});
	});
};

exports.logout = function(req, res){
	req.logout();
	res.redirect('/');
}