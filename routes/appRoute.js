var Application = require('../models/application');
var urlUtils = require('../utils/urlUtils');
var redirect = require('../utils/redirect');

exports.addApp = function(req, res){
	var app = new Application();
	app.preferences = {};
	app.user_id = req.user._id;
	app.name = req.body.name;
	app.base = req.body.base;

	app = urlUtils.createUrl(app, req.body);

	app.save(function(err, app){
		if(err){
			res.send(err);
		}

		res.json(app);
	});
}

//URL endpoint for get /apps
exports.getApps = function(req, res){
	Application.find({user_id: req.user._id}, function(err, apps){
		if(err){
			res.send(err);
		}

		res.json(apps);

	});
};

//URL endpoint for get /apps/:appId
exports.getApp = function(req, res){
	Application.findById(req.params.appId, function(err, app){
		if(err){
			res.send(err);
		}
		res.json(app);
	});
};

exports.updateApp = function(req, res){
	Application.findById(req.params.appId, function(err. app){
		if(err){
			res.send(err);
		}

		app.name = (req.body.name) ? req.body.name : app.name;
		app.base = (req.body.base) ? req.body.base : app.base;

		app = urlUtils.updateUrl(app, req.body);

		app.save(function(err, app){
			if(err){
				res.send(err);
			}

			res.json(app);
		});
	});
};

exports.deleteApp = function(req, res){
	Application.findByIdAndRemove(req.params.appId, function(err){
		if(err){
			res.send(err);
		}

		res.json({
			message: "App successfully deleted"
		});
	});
}

//-----------------  Redirecting apps -------------------

//URL endpoint for GET /:appBase

exports.appRedirect = function(req, res){
	var userAgent = req.useragent;
	Application.findOne({base: req.params.appBase}, function(err, app){
		if(err){
			res.send(err);
		}

		var redirectUrl = redirect.redirectUrl(app, userAgent);
		var redirectPage = redirect.generateRedirectPage(redirectUrl, 'app.html');
		res.send(redirectPage);
	});
}

