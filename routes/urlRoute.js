var Url = require('../models/url');
var Application = require('../models/application');

var urlUtils = require('../utils/urlUtils');
var redirect = require('../utils/redirect');

exports.get = function(req, res){
	res.render('index.html');
};

exports.addUrl = function(req, res){
	var url = new Url();
	url.user_id = req.user._id;
	url.app_id = req.body.app_id;
	urlUtils.validateUrlBase(url.app_id, req.body.base, function(err, urlBase){
		if(err){
			res.send(err);
		}
		url.base = urlBase;
		url = urlUtils.createUrl(url, req.body);
		url.save(function(err, app){
			if(err){
				res.send(err);
			}

			res.json(url);
		});
	});
};

//URL endpoint for GET /urls
exports.getUrls = function(req, res){
	Url.find({user_id: req.user._id}, function(err, urls){
		if(err){
			res.send(err);
		}

		res.json(urls);
	});
};

//URL endpoint for GET /:appId/urls
exports.getAppUrls = function(req, res){
	Url.find({app_id: req.params.appId}, function(err, urls){
		if(err){
			res.send(err);
		}

		res.json(urls);
	});
};

//URL endpoint for GET /:urlId
exports.getUrl = function(req, res){
	Url.findById(req.params.urlId, function(err, url){
		if(err){
			res.send(err);
		}

		res.json(url);
	});
};

exports.updateUrl = function(req, res){
	Url.findById(req.params.urlId, function(err, url){
		if(err){
			res.send(err);
		}

		url.base = (req.body.base) ? req.body.base : url.base;

		url = urlUtils.updateUrl(url, req.body);

		url.save(function(err, url){
			if(err){
				res.send(err);
			}

			res.json(url);
		});

	});
};

exports.deleteUrl = function(req, res){
	Url.findByIdAndRemove(req.params.urlId, function(err){
		if(err){
			res.send(err);
		}

		res.json({
			message: 'Url deleted successfully'
		});
	});
}
//--------------------  Redirecting Urls --------------------

//URL endpoint for get /:appBase/:urlBase

exports.urlRedirect = function(req, res){
	var userAgent = req.useragent;
	console.log(useragent);
	Application.findOne({base: req.params.appBase}, function(err, app){
		if(err){
			res.send(err);
		}

		Url.findOne({app_id: app._id, base: urlBase}, function(err, url){
			if(err){
				res.send(err);
			}

			var redirectUrl = redirect.redirectUrl(app, userAgent);
			var redirectPage = redirect.generateRedirectPage(redirectUrl, 'url.html');
			res.send(redirectPage);
		});
	});
};

//URL endpoint for get /:appBase/:urlBase/appnotinstalled
exports.urlAppNotFoundRedirect = function(req, res, next){
	return next();
}