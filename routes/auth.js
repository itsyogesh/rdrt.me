var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');

passport.serializeUser(function(user, callback){
	callback(null, user.id);
});

passport.deserializeUser(function(user, callback){
	User.findById(id, function(err, user){
		done(err, user);
	});
});

passport.use(new LocalStrategy(
	function (username, password, callback) {
		User.findOne({email: username}, function(err, user){
			if(err){
				return callback(err);
			}

			if(!user){
				return callback(null, false);
			}

			user.verifyPassword(password, function(err, isMatch){
				if(err){
					return callback(err);
				}

				if(!isMatch){
					return callback(null, false);
				}

				return callback(null, user);
			});

		});	
	}
));

exports.authenticate = passport.authenticate('local', {successRedirect: '/home', failureRedirect: '/login', failureFlash: true});
exports.isAuthenticated = passport.authenticate('local', {session: true});