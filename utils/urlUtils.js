
var Application = require('../models/application');
var Url = require('../models/url');

var deviceUrls = ['web_url', 'android_url', 'ios_url', 'windows_url'];
var preferences = ['web_active','ios_active', 'android_active', 'windows_active', 'default_url'];
var userAgentiOS = ['isiPad', 'isiPod', 'isiPhone'];
var userAgentAndroid = 'isAndroid';
var userAgentWindows = 'isWindows';

var urlUtils = {
	/*
	* Creates a url based on the argumets
	* @params urlObject: Object of url that needs to be created
	* @params details: req object
	*/

	createUrl: function(urlObject, details){
		
		deviceUrls.forEach(function(device){
			if(details[device]){
				urlObject[device] = details[device];
			}
		});

		preferences.forEach(function(pref){
			if(details[pref]){
				urlObject.preferences[pref] = details[pref];
			}
		});

		return urlObject;
	},

	updateUrl: function(urlObject, details){

		deviceUrls.forEach(function(device){
			if(details[device]){
				urlObject[device] = details[device];
			}
		});

		preferences.forEach(function(pref){
			if(details[pref]){
				urlObject.preferences[pref] = details[pref];
			}
		});
	},

	/*
	* Returns a url based on the user agent and default url
	* @params urlObject: Object of the url 
	* @params userAgent: User agent object
	*/

	redirectUrl: function(urlObject, userAgent){

		var url = urlObject.preferences.default_url;
		
		userAgentiOS.forEach(function(agent){
			if(userAgent[agent]){
				if(urlObject.preferences.ios_active){
					url = urlObject.ios_url;
				}
			}
		});
		
		if(userAgent[userAgentAndroid] && urlObject.preferences.android_active){
			url = urlObject.android_url;
		}

		else if(userAgent[userAgentWindows] && urlObject.preferences.windows_active){
			url = urlObject.windows_url;
		}

		else if(urlObject.preferences.web_active){
			url = urlObject.web_url;
		}

		return url;
	},

	validateUrlBase: function(app, urlBase, callback){
		Url.find({app_id: app}, function(err, urls){
			var error = null;
			if(err){
				return callback(err);
			}
			urls.forEach(function(url){
				if(url.base === urlBase){
					error = new Error('Url base already in use');
					return callback(error);
				}
			});

			return callback(null, urlBase);
		});
	}

};

module.exports = urlUtils;