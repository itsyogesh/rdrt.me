
var fs = require('fs');

var redirectPagesPath = '../public/redirect/';
var REDIRECT_URL_HOLDER = 'redirect_url_holder';

var redirect = {

	/*
	* Returns a url based on the user agent and default url
	* @params urlObject: Object of the url 
	* @params userAgent: User agent object
	*/

	redirectUrl: function(urlObject, userAgent){

		var url = urlObject.preferences.default_url;
		
		userAgentiOS.forEach(function(agent){
			if(userAgent[agent]){
				if(urlObject.preferences.ios_active){
					url = urlObject.ios_url;
				}
			}
		});
		
		if(userAgent[userAgentAndroid] && urlObject.preferences.android_active){
			url = urlObject.android_url;
		}

		else if(userAgent[userAgentWindows] && urlObject.preferences.windows_active){
			url = urlObject.windows_url;
		}

		else if(urlObject.preferences.web_active){
			url = urlObject.web_url;
		}

		return url;
	},

	/*
	* Returns an html string for the redirect page of the app
	* @params redirectUrl : Url to which the page should redirect.
	* @params pageName : Name of the template page
	*/

	generateRedirectPage: function(redirectUrl, pageName){
		var redirectPage = redirectPagesPath + pageName;
		var html = fs.readFileSync(redirectPage, 'utf-8').toString();
		html.replace(REDIRECT_URL_HOLDER, redirectUrl);

		return html;
	}

};

module.exports = redirect;