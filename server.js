var express = require('express');
var bodyParser = require('body-parser');
var useragent = require('express-useragent');
var mongoose = require('mongoose');
var flash = require('connect-flash');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var logger = require('morgan');
var passport = require('passport');

var routes = require('./routes/routes');

var app = express();

//Connect to mongodb
mongoose.connect('104.131.90.215:20102/swtch');
console.log('Connected to mongodb server');

//use static
app.use(express.static(__dirname + '/public'));


//Use logger
app.use(logger('dev'));

//require cookieparser
app.use(cookieParser());

//passport initialize
app.use(session({secret: 'redirection'}));

//Use passport
app.use(passport.initialize());

app.use(passport.session());

app.use(flash());

//Use body-parser
app.use(bodyParser.urlencoded({
    extended: false
}));

//Use useragent
app.use(useragent.express());

var port = process.env.PORT || 3000;

app.use('/', routes);

app.listen(port);

console.log("server started on " + port);